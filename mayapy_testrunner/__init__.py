#!/usr/bin/env python

import os
import sys
import subprocess


def main():
    '''
    Run all unittest tests using mayapy interpreter.
    Return 0 if nose result == True, 1 if False.
    '''
    test_dir = sys.argv[1:][0]
    python_dir = '/'.join(test_dir.split('/')[:-2:])
    rez_package_root = '/'.join(python_dir.split('/')[:-2:])
    os.environ['REZ_MAYA_PIPELINE_ROOT'] = rez_package_root
    pythonpath = os.environ['PYTHONPATH']
    os.environ['PYTHONPATH'] = '%s:%s' % (pythonpath, python_dir)

    cmd = 'mayapy -c'

    # start mayapy command string wrapper
    cmd += ' \''

    cmd += 'import os;'

    # If any modules discovered by nose do "from pymel.core import *", then pymel.internal.startup.initMEL
    # sources pluginPrefs.mel, which will load some plugins, some of which will segfault mayapy.
    # Setting $PYMEL_SKIP_MEL_INIT skips this:
    cmd += 'os.environ[\"PYMEL_SKIP_MEL_INIT\"] = \"1\";'

    # make sure maya.standalone is initialised here
    cmd += 'import maya.standalone;'
    cmd += 'maya.standalone.initialize();'
    cmd += 'import nose;'
    cmd += 'os.chdir(\"%s\");' % python_dir
    cmd += 'result=nose.run();'

    # a segfault will happen if we don't uninitialize()
    cmd += 'maya.standalone.uninitialize();'

    # nose result is a bool - return 0/1 exit code based on value
    cmd += 'os._exit(0 if result else 1)'

    # end mayapy command string wrapper
    cmd += '\''

    print 'maya test cmd:', cmd

    try:
        subprocess.check_call(cmd, shell=True)
        return 0
    except subprocess.CalledProcessError:
        return 1


if __name__ == "__main__":
    sys.exit(main())