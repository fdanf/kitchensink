from maya import cmds
from maya import OpenMaya as om


class BoundingBoxWorldPositions(object):
    """
    A utility class for returning various position vectors on a mesh's bounding box in world space
    """
    def __init__(self, geo):
        """
        @param geo: the transform node of a maya mesh
        """
        self._up_axis = 'Y'
        self._invert_axis = False
        self._bounds = {}
        self._axes = ['X', 'Y', 'Z']
        self._world_matrix = None
        self._scale = om.MVector()
        self._geo = geo
        self._set_bounds(geo)
        self._set_world_matrix()
        self._set_up_axis()

    def scale_x(self):
        """
        @rtype: float
        """
        return self._bounds['X'][1] - self._bounds['X'][0]

    def scale_y(self):
        """
        @rtype: float
        """
        return self._bounds['Y'][1] - self._bounds['Y'][0]

    def scale_z(self):
        """
        @rtype: float
        """
        return self._bounds['Z'][1] - self._bounds['Z'][0]

    def top_corner(self):
        """
        @return the world space position of the highest bounding box point
        @rtype: MVector()
        """
        return self._corner(top=True)

    def bottom_corner(self):
        """
        @return the world space position of the highest bounding box point
        @rtype: MVector()
        """
        return self._corner(bottom=True)

    def center(self):
        """
        @return the world space position of the bounding box xyz center
        @return: MVector()
        """
        return self._center()

    def top_center(self):
        """
        @return the world space position of the center of the top face
        @return: MVector()
        """
        return self._center(top=True)

    def bottom_center(self):
        """
        @return the world space position of the center of the top face
        @return: MVector()
        """
        return self._center(bottom=True)

    def world_points(self):
        """
        @return a list of all bounding box points as instances of MPoint()
        @rtype: list of MPoint()
        """
        p_min = om.MVector(self._bounds['X'][0], self._bounds['Y'][0], self._bounds['Z'][0])
        points = [0]*8
        points[0] = self._local_to_world(p_min)
        points[1] = self._local_to_world(p_min + om.MVector(self.scale_x(), 0, 0))
        points[2] = self._local_to_world(p_min + om.MVector(0, self.scale_y(), 0))
        points[3] = self._local_to_world(p_min + om.MVector(self.scale_x(), self.scale_y(), 0))
        points[4] = self._local_to_world(p_min + om.MVector(0, 0, self.scale_z()))
        points[5] = self._local_to_world(p_min + om.MVector(self.scale_x(), 0, self.scale_z()))
        points[6] = self._local_to_world(p_min + om.MVector(0, self.scale_y(), self.scale_z()))
        points[7] = self._local_to_world(p_min + om.MVector(self.scale_x(), self.scale_y(), self.scale_z()))
        return points

    def _center(self, top=False, bottom=False):
        """
        @return a position vector at the center of all axes, or top center, or bottom center
        @return: MVector()
        """
        local_center = om.MVector()

        for axis in self._axes:
            i = self._axes.index(axis)
            axis_center = self._center_axis(axis)
            axis_top = self._max(axis)
            axis_bottom = self._min(axis)

            # default to center for all axis
            local_center[i] = axis_center

            if top or bottom:
                # return the top face for the up axis
                if axis == self._up_axis:
                    if top:
                        local_center[i] = axis_top
                    if bottom:
                        local_center[i] = axis_bottom
                    if self._invert_axis:
                        local_center[i] = self._flip_axis(local_center[i], axis)

        return self._local_to_world(local_center)

    def _corner(self, top=False, bottom=False):
        points = self.world_points()
        y_values = []
        [y_values.append(p[1]) for p in points]

        if top and not bottom:
            top_point_y = sorted(y_values)[7]
        elif bottom and not top:
            top_point_y = sorted(y_values)[0]
        else:
            raise RuntimeError('Top or bottom kwargs must be true')

        local_position = [p for p in points if p[1] == top_point_y][0]
        return local_position

    def _flip_axis(self, face, axis):
        if face == self._min(axis):
            return self._max(axis)
        elif face == self._max(axis):
            return self._min(axis)

    def _local_to_world(self, translation):
        """
        @param: MVector()
        @return the world space position of a local translation vector
        @rtype: MVector()
        """
        old_matrix = om.MTransformationMatrix()
        old_matrix.setTranslation(translation, om.MSpace.kWorld)

        new_matrix = om.MTransformationMatrix(old_matrix.asMatrix() * self._world_matrix)
        new_translation = new_matrix.translation(om.MSpace.kWorld)
        return new_translation

    def _set_world_matrix(self):

        self._world_matrix = om.MMatrix(cmds.getAttr('%s.worldMatrix' % self._geo))

    def _set_bounds(self, geo):
        bbox = cmds.xform(geo, bb=True, q=True)

        self._bounds = {'X': (bbox[0], bbox[3]),
                        'Y': (bbox[1], bbox[4]),
                        'Z': (bbox[2], bbox[5])}

    def _center_axis(self, axis):
        """
        @return the center point of a given axis
        @rtype: float
        """
        return ((self._max(axis) - self._min(axis)) / 2) + self._min(axis)

    def _min(self, axis):
        """
        @:rtype: float
        """
        return self._bounds[axis][0]

    def _max(self, axis):
        """
        @:rtype: float
        """
        return self._bounds[axis][1]

    def _set_up_axis(self):
        """
        determine which local axis is 'up' in world space
        set self._upAxis and self._invertAxis accordingly
        """
        world_x_axis = om.MVector.kXaxisVector
        world_y_axis = om.MVector.kYaxisVector
        world_z_axis = om.MVector.kZaxisVector

        local_x_axis_worldspace = (world_x_axis * self._world_matrix).normal()
        local_y_axis_worldspace = (world_y_axis * self._world_matrix).normal()
        local_z_axis_worldspace = (world_z_axis * self._world_matrix).normal()

        local_x_dot_world_y = world_y_axis * local_x_axis_worldspace
        local_y_dot_world_y = world_y_axis * local_y_axis_worldspace
        local_z_dot_world_y = world_y_axis * local_z_axis_worldspace

        axis_dot_products = {'X': local_x_dot_world_y, 'Y': local_y_dot_world_y, 'Z': local_z_dot_world_y}

        up_axis_abs_value = sorted([abs(local_x_dot_world_y), abs(local_y_dot_world_y), abs(local_z_dot_world_y)])[2]

        # set up axis to the first local axis dot product matching colinearityValue
        # there could be several local axes equally close to world Y, but any will do
        for local_axis in axis_dot_products.keys():
            if abs(axis_dot_products[local_axis]) == up_axis_abs_value:
                self._up_axis = local_axis
                if axis_dot_products[local_axis] < 0:
                    self._invert_axis = True


class PolyBoundingBox(object):
    def __init__(self, reference_geo_transform, reference_geo_shape, polycube_path):
        self._reference_geo_transform = reference_geo_transform
        self._shape_node = reference_geo_shape
        self._polycube_path = polycube_path

    def place_vertices(self):
        """
        move the vertices of the polycube to the points of the reference geo bounding box
        i.e. don't change the polycube transform
        """
        print self._polycube_path
        polycube_shape = cmds.listRelatives(self._polycube_path, c=True, f=True, type='mesh')[0]

        reference_bbox = BoundingBoxWorldPositions(self._reference_geo_transform)
        dest_verts_world = reference_bbox.world_points()

        for x in dest_verts_world:
            i = dest_verts_world.index(x)

            new_vert_world = om.MVector(dest_verts_world[0])
            t = '%s.vtx[%s]' % (self._shape_node, i)
            local_offset = om.MVector(cmds.xform(t, query=True, translation=True, worldSpace=True))
            final_position = new_vert_world - local_offset

            print 'settingAttr: %s.vtx[%s]' % (polycube_shape, i)
            cmds.setAttr('%s.vtx[%s]' % (polycube_shape, i),
                         final_position[0], final_position[1], final_position[2])
