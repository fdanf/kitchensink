from maya import cmds
from maya import OpenMaya as om
from bounding_box import BoundingBoxWorldPositions


"""
usage:
lp = LocatorPlacement(mesh, locatorShape)
lp.scaleLocator()
lp.placeLocatorTopCenter()
"""


class LocatorPlacement(object):
    """
    Position and scale locator at a position in world space, by setting localPosition
    and localScale attributes, relative to the locator transform
    """
    def __init__(self, geo, locator_shape):
        """
        @:param geo: the transform node of a maya mesh
        @:param locatorShape: the shape node of a locator to be placed
        """
        self._locator = locator_shape
        self._scale = 10
        self._scale_factor = 0.5
        self._bounding_box = BoundingBoxWorldPositions(geo)

    def scale_locator(self):
        self._set_scale()
        self._scale_locator()

    def place_locator_top_center(self):
        world_position = self._bounding_box.top_center()
        parented_position = self._world_to_parented(world_position)
        self._place_locator(parented_position)

    def place_locator_bottom_center(self):
        world_position = self._bounding_box.bottom_center()
        parented_position = self._world_to_parented(world_position)
        self._place_locator(parented_position)

    def place_locator_center(self):
        world_position = self._bounding_box.center()
        parented_position = self._world_to_parented(world_position)
        self._place_locator(parented_position)

    def place_locator_top_corner(self):
        world_position = self._bounding_box.top_corner()
        parented_position = self._world_to_parented(world_position)
        self._place_locator(parented_position)

    def place_locator_bottom_corner(self):
        world_position = self._bounding_box.bottom_corner()
        parented_position = self._world_to_parented(world_position)
        self._place_locator(parented_position)

    def _place_locator(self, translation):
        cmds.setAttr('{0}.localPositionX'.format(self._locator), translation[0])
        cmds.setAttr('{0}.localPositionY'.format(self._locator), translation[1])
        cmds.setAttr('{0}.localPositionZ'.format(self._locator), translation[2])

    def _scale_locator(self):
        cmds.setAttr('{0}.localScaleX'.format(self._locator), self._scale)
        cmds.setAttr('{0}.localScaleY'.format(self._locator), self._scale)
        cmds.setAttr('{0}.localScaleZ'.format(self._locator), self._scale)

    def _set_scale(self):
        x = self._bounding_box.scale_x()
        y = self._bounding_box.scale_y()
        z = self._bounding_box.scale_z()
        self._scale = sorted([x, y, z], reverse=True)[0] * self._scale_factor

    def _world_to_parented(self, translation):
        """
        Given a position in world space, determine the local position the locator
        will need under it's parent
        @param translation: MVector()
        @rtype: MVector()
        """
        old_matrix = om.MTransformationMatrix()
        old_matrix.setTranslation(translation, om.MSpace.kWorld)
        locator_transform = cmds.listRelatives(self._locator, p=True, f=True)[0]
        parent = om.MMatrix(cmds.getAttr('{0}.matrix'.format(locator_transform)))
        new_matrix = om.MTransformationMatrix(old_matrix.asMatrix() * parent.inverse())
        new_translation = new_matrix.translation(om.MSpace.kWorld)
        return new_translation

