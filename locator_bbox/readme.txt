bounding box and locator placement utilities by daniel flood

requires maya 2014 or higher

Some utility classes I wrote to position a locator somewhere on the bounding box of a given mesh
(i.e. top corner in world space, top face in world space etc).  It does so via the "placement"
attributes on the locator shape node.  This is useful if the locator itself needs to store transforms
for nodes in a hierarchy, but we want to visually place it somewhere useful.

usage example:

1. create a locator and a polycube

2. run:

import sys
sys.path.append('path/to/locator_bbox/')
import locator_placement as lp
lp = LocatorPlacement('pCube1', 'locatorShape1')
lp.scaleLocator()
lp.placeLocatorTopCenter()

