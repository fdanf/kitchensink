import imath
import math

"""
Some wrappers to make dealing with ilmbase imath matrices easier.

Requires the python bindings for ilmbase.
"""

def float_equal(float1, float2, error=0.000001):
    return abs(float1-float2) < error


def imath_m44d_from_trs(t, r, s):
    """
    @rtype: imath.M44d matrix
    """
    translation = [t[0], t[1], t[2]]
    rotation = [r[0], r[1], r[2]]
    scale = [s[0], s[1], s[2]]

    tx, ty, tz = translation[0], translation[1], translation[2]
    rx, ry, rz = math.radians(rotation[0]), math.radians(rotation[1]), math.radians(rotation[2])
    sx, sy, sz = scale[0], scale[1], scale[2]

    e = tuple([0] * 4)
    translation = imath.M44d(e, e, e, (tx, ty, tz, 0))
    rotation = imath.Eulerd(rx, ry, rz).toMatrix44()
    scale = imath.M44d().setScale((sx, sy, sz))

    transformation_matrix = (scale * rotation) + translation

    values = [round(item, 8) for row in transformation_matrix for item in row]
    values = [values[0:4:], values[4:-8:], values[8:-4:], values[12::]]

    for row in values:
        row_index = values.index(row)
        for item in row:
            item_index = row.index(item)
            transformation_matrix[row_index][item_index] = item

    return transformation_matrix


class TransformationMatrix4x4(object):
    """
    Holds a reference to an imath m44d matrix, with utility
    methods such as returning a Transform object (translate, rotate, scale XYZ)
    """

    def __init__(self, M44d):
        self.matrix = M44d

    def get_matrix(self):
        """
        @rtype: imath M44d matrix
        """
        return self.matrix

    def set_matrix(self, M44d):
        self.matrix = M44d

    def get_translation(self):
        return self._get_translation()

    def get_rotation(self):
        r = self._get_rotation()
        return [r.x, r.y, r.z]

    def get_scale(self):
        s = self._get_scale()
        return [s.x, s.y, s.z]

    def as_list(self):
        return [item for row in self.matrix for item in row]

    def _get_translation(self):
        """
        @rtype: imath.V3d
        """
        translation = self.matrix.translation()
        return imath.V3d([translation[0], translation[1], translation[2]])

    def _get_rotation(self):
        """
        @rtype: imath.V3d
        """
        rotation = imath.V3d()
        tmp = imath.M44d()
        tmp.setValue(self.matrix)
        tmp.removeScaling()
        tmp.extractEulerXYZ(rotation)
        rotation = imath.V3d([round(math.degrees(r), 2) for r in rotation])
        return rotation

    def _get_scale(self):
        """
        @rtype: imath.V3d
        """
        scale = imath.V3d()
        self.matrix.extractScaling(scale)
        scale = imath.V3d([round(s, 2) for s in scale])
        return scale

    def __str__(self):
        m = [item for row in self.matrix for item in row]

        output = "[ %s %s %s %s\n %s %s %s %s\n %s %s %s %s\n %s %s %s %s ]" % \
                 (m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8],
                  m[9], m[10], m[11], m[12], m[13], m[14], m[15])

        return output

    def __eq__(self, other):
        return self == other

    def __ne__(self, other):
        return not self.__eq__(other)
