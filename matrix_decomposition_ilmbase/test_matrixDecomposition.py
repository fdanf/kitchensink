import unittest
import testMatrices
import matrixmath


class MatrixDecompositionTestCase(unittest.TestCase):
    def setUp(self):
        self.matrices = testMatrices.get()

    def test__getTranslation(self):
        for key_pair in self.matrices:
            matrix = key_pair[0]
            trs = key_pair[1]
            t = matrixmath.TransformationMatrix4x4(matrix)
            result = t.get_translation()
            expected = [trs[0], trs[1], trs[2]]
            self.assertTrue(result.__eq__(expected))

    def test__getRotation(self):
        for key_pair in self.matrices:
            matrix = key_pair[0]
            trs = key_pair[1]
            t = matrixmath.TransformationMatrix4x4(matrix)
            result = t.get_rotation()
            expected = [trs[3], trs[4], trs[5]]
            self.assertTrue(result.__eq__(expected))

    def test__getScale(self):
        for key_pair in self.matrices:
            matrix = key_pair[0]
            trs = key_pair[1]
            t = matrixmath.TransformationMatrix4x4(matrix)
            result = t.get_scale()
            expected = [trs[6], trs[7], trs[8]]
            self.assertTrue(result.__eq__(expected))

    def test_getTranslationRotationScale_roundTrip(self):
        for key_pair in self.matrices:
            matrix = key_pair[0]
            t = matrixmath.TransformationMatrix4x4(matrix)
            result = matrixmath.imath_m44d_from_trs(t.get_translation(), t.get_rotation(), t.get_scale())
            expected = t
            self.assertTrue(result.__eq__(expected))

